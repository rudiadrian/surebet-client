var gulp = require('gulp');

var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var uglifycss = require('gulp-uglifycss');
var coffeelint = require('gulp-coffeelint');
var coffee = require('gulp-coffee');
var less = require('gulp-less');
var prettify = require('gulp-html-prettify');
var jade = require('gulp-jade');
var changed = require('gulp-changed');

var hidden_files = '**/_*.*';
var ignored_files = '!' + hidden_files;

// SOURCES
var vendorBaseScripts = [
    'js/vendor/angular.min.js',
    'js/vendor/angular-ui-router.min.js',
    'js/vendor/loading-bar.js',
    'js/vendor/angular-cookies.min.js',
    'js/vendor/angular-animate.min.js',
    'js/vendor/toaster.js',
    'js/vendor/angular-translate.min.js',
    'js/vendor/angular-translate-loader-static-files.js',
    'js/vendor/angular-resource.min.js',
    'js/vendor/ng-table.js',
    'js/vendor/ui-bootstrap-tpls-0.12.0.min.js',
    'js/vendor/Chart.js',
    'js/vendor/tc-angular-chartjs.min.js',
    'js/vendor/angular-filter.min.js',
    'js/vendor/angular-poller.min.js',
    'js/vendor/underscore-min.js',
    'js/vendor/moment.min.js'
];

var source = {
    scripts: {
        app: 'js/src/**/*.coffee',
        vendor: vendorBaseScripts,
        watch: ['js/src/**/*.coffee']
    },
    templates: {
        app: {
            files: ['jade/index.jade'],
            watch: ['jade/index.jade']
        },
        pages: {
            files: ['jade/pages/*.jade'],
            watch: ['jade/pages/*.jade']
        },
        views: {
            files: ['jade/views/*.jade', 'jade/views/**/*.jade', ignored_files],
            watch: ['jade/views/**/*.jade']
        }
    },
    styles: {
        app: {
            main: ['less/app.less', '!less/themes/*.less'],
            dir: 'less',
            watch: ['less/*.less', '!less/**/*.less', '!less/themes/*.less']
        },
        theme: {
            main: 'less/theme/sb-admin-2.less',
            dir: 'less/theme',
            watch: ['less/theme/*.less']
        }
    }
};

// BUILD TARGET
var build = {
    scripts: {
        app: {
            main: 'app.js',
            dir: '../static/js'
        },
        vendor: {
            main: 'base.js',
            dir: '../static/js'
        }
    },
    templates: {
        app: '..',
        pages: '../templates/pages',
        views: '../templates/views'
    },
    styles: '../static/css'
};

// LINT COFFEESCRIPT
gulp.task('scripts:lint', function () {
    "use strict";
    return gulp.src(source.scripts.app)
        .pipe(coffeelint())
        .pipe(coffeelint.reporter('default'));
});

// JS APP
gulp.task('scripts:app', function () {
    "use strict";
    return gulp.src(source.scripts.app)
        .pipe(coffee({bare: true}))
        .pipe(concat(build.scripts.app.main))
        .pipe(uglify())
        .pipe(gulp.dest(build.scripts.app.dir));
});

// JS VENDOR
gulp.task('scripts:vendor', function () {
    "use strict";
    return gulp.src(source.scripts.vendor)
        .pipe(uglify())
        .pipe(concat(build.scripts.vendor.main))
        .pipe(gulp.dest(build.scripts.vendor.dir));
});

// JADE APP
gulp.task('templates:app', function () {
    "use strict";
    return gulp.src(source.templates.app.files)
        .pipe(changed(build.templates.app, { extension: 'html' }))
        .pipe(jade())
        .pipe(prettify({
            indent_char: ' ',
            indent_size: 3,
            unformatted: ['a', 'sub', 'sup', 'b', 'i', 'u']
        }))
        .pipe(gulp.dest(build.templates.app));
});

// JADE PAGES
gulp.task('templates:pages', function () {
    "use strict";
    return gulp.src(source.templates.pages.files)
        .pipe(changed(build.templates.pages, { extension: '.html' }))
        .pipe(jade())
        .pipe(prettify({
            indent_char: ' ',
            indent_size: 3,
            unformatted: ['a', 'sub', 'sup', 'b', 'i', 'u']
        }))
        .pipe(gulp.dest(build.templates.pages));
});

// JADE VIEWS
gulp.task('templates:views', function () {
    "use strict";
    return gulp.src(source.templates.views.files)
        .pipe(changed(build.templates.views, { extension: '.html' }))
        .pipe(jade())
        .pipe(prettify({
            indent_char: ' ',
            indent_size: 3,
            unformatted: ['a', 'sub', 'sup', 'b', 'i', 'u']
        }))
        .pipe(gulp.dest(build.templates.views));
});

// LESS APP
gulp.task('styles:app', function () {
    "use strict";
    return gulp.src(source.styles.app.main)
        .pipe(less({
            paths: [source.styles.app.dir]
        }))
        .pipe(uglifycss())
        .pipe(gulp.dest(build.styles));
});

// LESS THEME
gulp.task('styles:theme', function () {
    "use strict";
    return gulp.src(source.styles.theme.main)
        .pipe(less({
            paths: [source.styles.theme.dir]
        }))
        .pipe(uglifycss())
        .pipe(gulp.dest(build.styles));
});

// WATCH
gulp.task('watch', function () {
    "use strict";
    gulp.watch(source.scripts.watch, ['scripts:app']);
    gulp.watch(source.templates.app.watch, ['templates:app']);
    gulp.watch(source.templates.pages.watch, ['templates:pages']);
    gulp.watch(source.templates.views.watch, ['templates:views']);
});

// DEFAULT TASK
gulp.task('default', [
    'scripts:lint',
    'scripts:vendor',
    'scripts:app',
    'templates:app',
    'templates:pages',
    'templates:views',
    'styles:app',
    'styles:theme',
    'watch'
]);