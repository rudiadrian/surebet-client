NotificationController = ($scope
                Notification
                $stateParams
                $rootScope) ->
  notificationID = $stateParams.id
  Notification.mark_as_read({id: notificationID}).$promise.then (result) ->
    $rootScope.$emit('readNotification')
  $scope.notification = Notification.get({id: notificationID})
  return

NotificationController.$inject = [
  '$scope'
  'Notification'
  '$stateParams'
  '$rootScope'
]


app.controller('NotificationController', NotificationController)