SidebarController = ($scope, League) ->
  vm = @
  League.query().$promise.then (response) ->
    vm.leagues = response
  return

SidebarController.$inject = [
  '$scope'
  'League'
]

app.controller('SidebarController', SidebarController)