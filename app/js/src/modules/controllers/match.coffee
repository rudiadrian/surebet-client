MatchController = (
  $scope
  Match
  $stateParams
  MatchAnalysisChart) ->

  matchID = $stateParams.id
  Match.get({id: matchID}).$promise.then (result) ->
    $scope.match = result
    if $scope.match.home_score == null and $scope.match.away_score == null
      Match.analysis({id: matchID}).$promise.then (result) ->
        $scope.analysis = result
        $scope.analysisChart = new MatchAnalysisChart($scope.analysis)
  $scope.home_players = Match.home_players({id: matchID})
  $scope.away_players = Match.away_players({id: matchID})
  $scope.previous_matches = Match.previous_matches({id: matchID})
  return

MatchController.$inject = [
  '$scope'
  'Match'
  '$stateParams'
  'MatchAnalysisChart'
]

app.controller('MatchController', MatchController)