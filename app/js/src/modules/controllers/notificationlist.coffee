NotificationListController = ($scope
                              Notification,
                              $filter
                              $rootScope) ->
  Notification.query().$promise.then (notifications) ->
    Notification.mark_all_as_read().$promise.then (result) ->
      $rootScope.$emit('readNotification')
    $scope.notifications = {}
    angular.forEach notifications, (notification) ->
      date = $filter('date')(notification.timestamp, 'dd-MM-yyyy')
      if $scope.notifications[date] == undefined
        $scope.notifications[date] = []
      $scope.notifications[date].push notification
      return
    return
  return

NotificationListController.$inject = [
  '$scope'
  'Notification'
  '$filter'
  '$rootScope'
]


app.controller('NotificationListController', NotificationListController)