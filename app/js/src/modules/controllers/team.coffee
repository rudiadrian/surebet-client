TeamController = ($scope
                  Team
                  $stateParams) ->

  teamID = $stateParams.id
  $scope.players = Team.players({id: teamID})
  $scope.currentCompetitions = Team.current_competitions({id: teamID})
  $scope.last_matches =
    overall: Team.last_matches({id: teamID})
    home: Team.last_home_matches({id: teamID})
    away: Team.last_away_matches({id: teamID})
  Team.get({id: teamID}).$promise.then (team) ->
    $scope.team = team
    return


  return

TeamController.$inject = [
  '$scope'
  'Team'
  '$stateParams'
]

app.controller('TeamController', TeamController)