NotificationsController = ($scope
                           Notification
                           poller
                           toaster
                           $filter
                           $rootScope) ->

  $scope.notifications = Notification.query()
  callback = (result) ->
    if result.length > 0
      angular.forEach result, (notification) ->
        $scope.notifications.unshift notification
        toaster.pop(
          'info'
          'New notification'
          $filter('truncate')(notification.description, 25, '...')
          )
        return
    return
  notificationsPoller = poller.get Notification, {
    action: 'watch_notifications'
    delay: 5000
    smart: 'true'
  }
  notificationsPoller.promise.then null, null, callback
  $rootScope.$on 'readNotification', (event) ->
    $scope.notifications = Notification.query()
  return

NotificationsController.$inject = [
  '$scope'
  'Notification'
  'poller'
  'toaster'
  '$filter'
  '$rootScope'
]

app.controller('NotificationsController', NotificationsController)