StandingsController = ($scope
                       Country
                       $stateParams
                       LeagueSeason
                       Match) ->

  countryID = $stateParams.country_id
  $scope.country = Country.get({id: countryID})
  $scope.leagues = Country.leagues({id: countryID})

  $scope.sortOverallStandings = ->
    $scope.standings =
      _.sortBy $scope.standings, (team) ->
        return team.points * -1

  $scope.sortHomeStandings = ->
    $scope.standings =
      _.sortBy $scope.standings, (team) ->
        return team.home_points * -1

  $scope.sortAwayStandings = ->
    $scope.standings =
      _.sortBy $scope.standings, (team) ->
        return team.away_points * -1

  clearSelectedSeason = ->
    $scope.selectedSeason = null
    $scope.standings = null
    $scope.matches = null

  clearSelectedDates = ->
    $scope.selectedWeek = null
    $scope.matches = null

  $scope.computeAvailableYears = ->
    season = $scope.selectedLeagueSeason.season
    $scope.availableYears = []
    if season.start_year == season.end_year
      year =
        year: season.start_year
        weeks: []
      startWeek = moment($scope.selectedLeagueSeason.start_date).week()
      endWeek = moment($scope.selectedLeagueSeason.start_date).week()
      year.weeks = [startWeek..endWeek]
      $scope.availableYears.push year
    else
      year =
        year: season.start_year
        weeks: []
      startWeek = moment($scope.selectedLeagueSeason.start_date).week()
      endWeek = 52
      year.weeks = [startWeek..endWeek]
      $scope.availableYears.push year
      year =
        year: season.end_year
        weeks: []
      endWeek = moment($scope.selectedLeagueSeason.end_date).week()
      startWeek = 1
      year.weeks = [startWeek..endWeek]
      $scope.availableYears.push year

  $scope.$watch 'selectedLeague', (newValue, oldValue) ->
    if !(newValue?)
      clearSelectedSeason()
      clearSelectedDates()
      return
    return

  $scope.$watch 'selectedSeason', (newValue, oldValue) ->
    if newValue? and newValue != oldValue
      LeagueSeason.query({
        leagueID: $scope.selectedLeague.id
        seasonID: $scope.selectedSeason.id
      }).$promise.then (response) ->
        $scope.selectedLeagueSeason = response[0]
        LeagueSeason.standings({id: $scope.selectedLeagueSeason.id})
        .$promise.then (response) ->
          $scope.standings = response
          $scope.sortOverallStandings()
          $scope.computeAvailableYears()
      return
    else
      clearSelectedSeason()
      clearSelectedDates()
      return
    return

  $scope.$watch 'selectedYear', (newValue, oldValue) ->
    if !(newValue?)
      clearSelectedDates()
      return
    return

  $scope.$watch 'selectedWeek', (newValue, oldValue) ->
    if newValue? and newValue != oldValue
      Match.query({
        year: $scope.selectedYear.year
        week: $scope.selectedWeek
        league_season: $scope.selectedLeagueSeason.id
      }).$promise.then (response) ->
        $scope.matches = response.results
      return
    else
      clearSelectedDates()
      return
    return

  return

StandingsController.$inject = [
  '$scope'
  'Country'
  '$stateParams'
  'LeagueSeason'
  'Match'
]

app.controller('StandingsController', StandingsController)