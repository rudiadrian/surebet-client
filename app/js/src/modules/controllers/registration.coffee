RegistrationController = ($scope, djangoSessionAuth, toaster, $translate) ->
  do prepareTranslations = ->
    $translate('registration.account_created').then (translation) ->
      $scope.ACCOUNT_CREATED_STRING = translation
      return

    $translate('messages.success').then (translation) ->
      $scope.SUCCESS_STRING = translation
      return

    return

  do initController = ->
    $scope.user =
      username: ''
      first_name: ''
      last_name: ''
      password: ''
    $scope.errors = []
    return

  $scope.registerNewUser = (registrationForm) ->
    djangoSessionAuth.register($scope.user)
    .then ->
      initController()
      toaster.pop(
        'success'
        $scope.SUCCESS_STRING
        $scope.ACCOUNT_CREATED_STRING
      )
      return
    , (rejection) ->
      $scope.errors = rejection
      return
    return

RegistrationController.$inject = [
  '$scope'
  'djangoSessionAuth'
  'toaster'
  '$translate'
]

app.controller('RegistrationController', RegistrationController)