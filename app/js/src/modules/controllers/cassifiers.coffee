ClassifiersController = ($scope
  Analyse
  $rootScope
  djangoSessionAuth
  toaster
  $translate) ->

  $scope.user = angular.copy $rootScope.loggedUser
  $scope.analyses = Analyse.query()

  do prepareTranslations = ->
    $translate('messages.success').then (translation) ->
      $scope.SUCCESS_STRING = translation
      return

    $translate('profile.profile_updated').then (translation) ->
      $scope.PROFILE_UPDATED_STRING = translation
      return

    return

  $scope.updateProfile = (profileForm) ->
    djangoSessionAuth.updateProfile($scope.user)
    .then ->
      $scope.errors = []
      toaster.pop(
        'success'
        $scope.SUCCESS_STRING
        $scope.PROFILE_UPDATED_STRING
      )
      $rootScope.loggedUser = angular.copy $scope.user
    , (rejected) ->
      $scope.errors = rejected

  $scope.$watch 'selectedClassifier', (newValue, oldValue) ->
    if newValue != oldValue
      $scope.user.userprofile.classifier = newValue.id
      $scope.updateProfile()
    return

  return

ClassifiersController.$inject = [
  '$scope'
  'Analyse'
  '$rootScope'
  'djangoSessionAuth'
  'toaster'
  '$translate'
]

app.controller('ClassifiersController', ClassifiersController)