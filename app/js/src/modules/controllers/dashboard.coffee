DashboardController = ($scope, MatchAnalysis, Notification) ->
  $scope.highestOutcomes = MatchAnalysis.highest_outcomes()
  $scope.notifications = Notification.query()
  return

DashboardController.$inject = [
  '$scope'
  'MatchAnalysis'
  'Notification'
]


app.controller('DashboardController', DashboardController)