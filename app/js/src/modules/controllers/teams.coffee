TeamsController = ($scope, Team, ngTableParams, $filter) ->
  $scope.filters =
    name: ''
    country: ''
  $scope.sorting =
    name: 'asc'
    country: 'asc'
  $scope.tableParams = new ngTableParams({
    filter: $scope.filters
    sorting: $scope.sorting
    page: 1
    count: 20
  },
    counts: []
    total: 0
    getData: ($defer, params) ->
      Team.query({
        page: params.page()
        name: $scope.filters.name
        country: $scope.filters.country
      }).$promise.then (response) ->
        teams = response.results
        if params.sorting()
          teams = $filter('orderBy')(teams, params.orderBy())
        params.total response.count
        $defer.resolve teams
        return
  )
  return

TeamsController.$inject = [
  '$scope'
  'Team'
  'ngTableParams'
  '$filter'
]

app.controller('TeamsController', TeamsController)