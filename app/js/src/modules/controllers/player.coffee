PlayerController = ($scope
                    Player
                    $stateParams
                    PlayerFormChart) ->
  playerID = $stateParams.id
  $scope.player = Player.get({id: playerID})
  $scope.currentCompetitionsStatistics =
    Player.current_competitions({id: playerID})
  Player.last_matches({id: playerID}).$promise.then (result) ->
    $scope.lastMatches = result
    $scope.playerFormChart = new PlayerFormChart($scope.lastMatches)
  return

PlayerController.$inject = [
  '$scope'
  'Player'
  '$stateParams'
  'PlayerFormChart'
]

app.controller('PlayerController', PlayerController)