PredictionsController = ($scope, MatchAnalysis, ngTableParams, $filter) ->
  $scope.filters =
    league: ''
  $scope.sorting =
    league: 'asc'
    home_win_outcome: 'desc'
    draw_outcome: 'desc'
    away_win_outcome: 'desc'
  $scope.tableParams = new ngTableParams({
    filter: $scope.filters
    sorting: $scope.sorting
    page: 1
    count: 20
  },
    counts: []
    total: 0
    getData: ($defer, params) ->
      MatchAnalysis.query({
        page: params.page()
        league: $scope.filters.league
      }).$promise.then (response) ->
        predictions = response.results
        if params.sorting()
          predictions = $filter('orderBy')(predictions, params.orderBy())
        params.total response.count
        $defer.resolve predictions
        return
  )
  return

PredictionsController.$inject = [
  '$scope'
  'MatchAnalysis'
  'ngTableParams'
  '$filter'
]


app.controller('PredictionsController', PredictionsController)