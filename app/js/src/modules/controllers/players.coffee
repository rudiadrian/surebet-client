PlayersController = ($scope, Player, ngTableParams, $filter) ->
  $scope.filters =
    full_name: ''
    current_team: ''
    nationality: ''
  $scope.sorting =
    full_name: 'asc'
  $scope.tableParams = new ngTableParams({
    filter: $scope.filters
    sorting: $scope.sorting
    page: 1
    count: 20
  },
    counts: []
    total: 0
    getData: ($defer, params) ->
      Player.query({
        page: params.page()
        full_name: $scope.filters.full_name
        current_team: $scope.filters.current_team
        nationality: $scope.filters.nationality
      }).$promise.then (response) ->
        players = response.results
        if params.sorting()
          players = $filter('orderBy')(players, params.orderBy())
        params.total response.count
        $defer.resolve players
        return
  )
  return

PlayersController.$inject = [
  '$scope'
  'Player'
  'ngTableParams'
  '$filter'
]

app
  .controller('PlayersController', PlayersController)