AccountController = ($rootScope
                     $scope
                     $state
                     djangoSessionAuth
                     toaster
                     $translate) ->

  $scope.user = angular.copy $rootScope.loggedUser
  $scope.errors = []

  do prepareTranslations = ->
    $translate('messages.success').then (translation) ->
      $scope.SUCCESS_STRING = translation
      return

    $translate('profile.profile_updated').then (translation) ->
      $scope.PROFILE_UPDATED_STRING = translation
      return

    return
    
  $scope.updateProfile = (profileForm) ->
    djangoSessionAuth.updateProfile($scope.user)
    .then ->
      $scope.errors = []
      toaster.pop(
        'success'
        $scope.SUCCESS_STRING
        $scope.PROFILE_UPDATED_STRING
      )
      $rootScope.loggedUser = angular.copy $scope.user
    , (rejected) ->
      $scope.errors = rejected

  $scope.logout = () ->
    djangoSessionAuth.logout()
      .then ->
        $state.go('page.login')

AccountController.$inject = [
  '$rootScope'
  '$scope'
  '$state'
  'djangoSessionAuth'
  'toaster'
  '$translate'
]

app.controller('AccountController', AccountController)