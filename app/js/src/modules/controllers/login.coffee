LoginController = ($scope, $state, djangoSessionAuth) ->
  do initController = ->
    $scope.user =
      username: ''
      password: ''
    $scope.errors = []

  $scope.login = (formData) ->
    djangoSessionAuth.login($scope.user.username, $scope.user.password)
    .then ->
      $state.go('app.dashboard')
    , (rejection) ->
      $scope.errors = rejection

LoginController.$inject = [
  '$scope'
  '$state'
  'djangoSessionAuth',
]

app.controller('LoginController', LoginController)