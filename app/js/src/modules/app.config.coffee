config = ($stateProvider, $urlRouterProvider, $httpProvider) ->
  viewsBasepath = (uri) ->
    return 'templates/views/' + uri

  pagesBasepath = (uri) ->
    return 'templates/pages/' + uri

  $stateProvider
    .state 'app',
      url: '/app'
      templateUrl: viewsBasepath('app.html')
      resolve: {
        authenticated: ['djangoSessionAuth', (djangoSessionAuth) ->
          djangoSessionAuth.getAuthenticationStatusPromise()
        ]
      }

    .state 'app.dashboard',
      url: '/dashboard'
      templateUrl: viewsBasepath('dashboard.html')
      title: 'dashboard.dashboard'
      controller: 'DashboardController'

    .state 'app.classifiers',
      url: '/classifiers'
      templateUrl: viewsBasepath('classifiers.html')
      title: 'classifiers.classifiers'
      controller: 'ClassifiersController'

    .state 'app.profile',
      url: '/profile'
      templateUrl: viewsBasepath('profile.html')
      title: 'profile.profile'
      controller: 'AccountController'

    .state 'app.players',
      url: '/players'
      templateUrl: viewsBasepath('players.html')
      title: 'players.players'
      controller: 'PlayersController'

    .state 'app.player',
      url: '/players/:id'
      templateUrl: viewsBasepath('player.html')
      title: 'players.player_statistics'
      controller: 'PlayerController'

    .state 'app.teams',
      url: '/teams'
      templateUrl: viewsBasepath('teams.html')
      title: 'teams.teams'
      controller: 'TeamsController'

    .state 'app.team',
      url: '/teams/:id'
      templateUrl: viewsBasepath('team.html')
      title: 'teams.team_statistics'
      controller: 'TeamController'

    .state 'app.notifications',
      url: '/notifications'
      templateUrl: viewsBasepath('notifications.html')
      title: 'notifications.notifications'
      controller: 'NotificationListController'

    .state 'app.notification',
      url: '/notifications/:id'
      templateUrl: viewsBasepath('notification.html')
      title: 'notifications.notification'
      controller: 'NotificationController'

    .state 'app.standings',
      url: '/standings/:country_id'
      templateUrl: viewsBasepath('standings.html')
      title: 'standings.standings'
      controller: 'StandingsController'

    .state 'app.predictions',
      url: '/predictions/:id'
      templateUrl: viewsBasepath('predictions.html')
      title: 'predictions.predictions'
      controller: 'PredictionsController'

    .state 'app.match',
      url: '/matches/:id'
      templateUrl: viewsBasepath('match.html')
      title: 'matches.match'
      controller: 'MatchController'

    .state 'page',
      url: '/page'
      templateUrl: pagesBasepath('page.html')

    .state 'page.login',
      url: '/login'
      templateUrl: pagesBasepath('login.html')
      controller: 'LoginController'
      title: 'login.login'

    .state 'page.registration',
      url: '/registration'
      templateUrl: pagesBasepath('registration.html')
      controller: 'RegistrationController'
      title: 'registration.registration'

  $urlRouterProvider.otherwise('app/dashboard')

  $httpProvider.defaults.xsrfCookieName = 'csrftoken'
  $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken'
  $httpProvider.defaults.withCredentials = true


config.$inject = [
  '$stateProvider'
  '$urlRouterProvider'
  '$httpProvider'
]

stateChangeErrorWatcher = ($rootScope, $state, APP_ERRORS) ->
  $rootScope.$on '$stateChangeError', (event, toState,
                                       toParams, fromState,
                                       fromParams, error) ->
    if error == APP_ERRORS.not_logged_in
      $state.go('page.login')
    return

stateChangeErrorWatcher.$inject = [
  '$rootScope'
  '$state'
  'APP_ERRORS'
]

stateChangeSuccessWatcher = ($rootScope, $state, $translate) ->
  $rootScope.$on '$stateChangeSuccess', ->
    $translate($state.current.title).then (translation) ->
      $rootScope.currentPageTitle = translation

stateChangeSuccessWatcher.$inject = [
  '$rootScope'
  '$state'
  '$translate'
]

app
  .config(config)
  .run(stateChangeErrorWatcher)
  .run(stateChangeSuccessWatcher)