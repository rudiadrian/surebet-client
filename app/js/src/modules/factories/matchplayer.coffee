MatchPlayer = ($resource, APP_URLS) ->
  return $resource APP_URLS.api + '/match_players/:id', id: '@id'

MatchPlayer.$inject = [
  '$resource'
  'APP_URLS'
]

app.factory('MatchPlayer', MatchPlayer)