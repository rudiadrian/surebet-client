PlayerFormChart = ($filter)->
  playerFormChart = (lastMatches) ->
    that = @
    ratings = []
    dates = []

    angular.forEach lastMatches, (match) ->
      ratings.push match.rating
      dates.push $filter('date')(match.match.datetime, 'dd-MM-yyyy')

    ratings.reverse()
    dates.reverse()

    that.data =
      labels: dates
      datasets: [
        {
          fillColor: 'rgba(220,220,220,0.2)'
          strokeColor: 'rgba(220,220,220,1)'
          pointColor: 'rgba(220,220,220,1)'
          pointStrokeColor: '#fff'
          pointHighlightFill: '#fff'
          pointHighlightStroke: 'rgba(220,220,220,1)'
          data: ratings
        }
      ]

    that.options =
      responsive: true
      scaleShowGridLines : true
      scaleGridLineColor : "rgba(0,0,0,.05)"
      scaleGridLineWidth : 1
      bezierCurve : true
      bezierCurveTension : 0.4
      pointDot : true
      pointDotRadius : 4
      pointDotStrokeWidth : 1
      pointHitDetectionRadius : 20
      datasetStroke : true
      datasetStrokeWidth : 2
      datasetFill : true
    return

  return playerFormChart

PlayerFormChart.$inject = [
  '$filter'
]

app.factory('PlayerFormChart', PlayerFormChart)