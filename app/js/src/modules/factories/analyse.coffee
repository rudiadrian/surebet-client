Analyse = ($resource, APP_URLS) ->
  return $resource APP_URLS.api + '/analyses/:id', {id: '@id'}

Analyse.$inject = [
  '$resource'
  'APP_URLS'
]

app.factory('Analyse', Analyse)