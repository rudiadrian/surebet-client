League = ($resource, APP_URLS) ->
  return $resource APP_URLS.api + '/leagues/:id', {id: '@id'}, {
    seasons:
      method: 'GET'
      url: APP_URLS.api + '/leagues/:id/seasons'
      params: {id: '@id'}
      isArray: true
  }

League.$inject = [
  '$resource'
  'APP_URLS'
]

app.factory('League', League)