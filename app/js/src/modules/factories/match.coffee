Match = ($resource, APP_URLS) ->
  return $resource APP_URLS.api + '/matches/:id/', {
    id: '@id'
    year: '@year'
    week: '@week'
    league_season: '@league_season'
  }, {
    query:
      method: 'GET'
      isArray: false
    home_players:
      method: 'GET'
      url: APP_URLS.api + '/matches/:id/home_players'
      params: { id: '@id' }
      isArray: true
    away_players:
      method: 'GET'
      url: APP_URLS.api + '/matches/:id/away_players'
      params: { id: '@id' }
      isArray: true
    previous_matches:
      method: 'GET'
      url: APP_URLS.api + '/matches/:id/previous_matches'
      params: { id: '@id' }
      isArray: true
    analysis:
      method: 'GET'
      url: APP_URLS.api + '/matches/:id/analyse'
      params: { id: '@id' }
      isArray: false
  }

Match.$inject = [
  '$resource'
  'APP_URLS'
]

app.factory('Match', Match)