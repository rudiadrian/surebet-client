Player = ($resource, APP_URLS) ->
  return $resource APP_URLS.api + '/players/:id/', id: '@id', {
    query:
      method: 'GET'
      params:
        id: '@id'
        page: '@page'
      url: APP_URLS.api + '/players/:id/?page=:page'
      isArray: false

    current_competitions:
      method: 'GET'
      params: {id: '@id'}
      url: APP_URLS.api + '/players/:id/current_competitions/'
      isArray: true

    last_matches:
      method: 'GET'
      params: {id: '@id'}
      url: APP_URLS.api + '/players/:id/last_matches/'
      isArray: true
  }

Player.$inject = ['$resource', 'APP_URLS']

app
  .factory('Player', Player)