Team = ($resource, APP_URLS) ->
  return $resource APP_URLS.api + '/teams/:id', id: '@id', {
    query:
      method: 'GET'
      params:
        id: '@id'
        page: '@page'
      url: APP_URLS.api + '/teams/:id/?page=:page'
      isArray: false

    players:
      url: APP_URLS.api + '/teams/:id/players'
      method: 'GET'
      params: {id: '@id'}
      isArray: true

    last_matches:
      url: APP_URLS.api + '/teams/:id/latest_matches'
      method: 'GET'
      params: {id: '@id'}
      isArray: true

    last_home_matches:
      url: APP_URLS.api + '/teams/:id/last_home_matches'
      method: 'GET'
      params: {id: '@id'}
      isArray: true

    last_away_matches:
      url: APP_URLS.api + '/teams/:id/last_away_matches'
      method: 'GET'
      params: {id: '@id'}
      isArray: true

    current_competitions:
      url: APP_URLS.api + '/teams/:id/competitions'
      method: 'GET'
      params: {id: '@id'}
      isArray: true
  }

Team.$inject = [
  '$resource'
  'APP_URLS'
]

app.factory('Team', Team)