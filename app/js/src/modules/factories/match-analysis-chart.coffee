MatchAnalysisChart = ->
  matchAnalysisChart = (prediction) ->
    that = @

    that.data = [
      {
        value: parseFloat(prediction.home_win_outcome)
        color: '#F7464A'
        highlight: '#FF5A5E'
        label: "Wygrana gospodarzy"
      }
      {
        value: parseFloat(prediction.draw_outcome)
        color: '#46BFBD'
        highlight: '#5AD3D1'
        label: "Remis"
      }
      {
        value: parseFloat(prediction.away_win_outcome)
        color: '#FDB45C'
        highlight: '#FFC870'
        label: "Wygrana gości"
      }
    ]

    that.options =
      segmentShowStroke : true
      segmentStrokeColor : '#fff'
      segmentStrokeWidth : 2
      percentageInnerCutout : 50
      animationSteps : 100
      animationEasing : 'easeOutBounce'
      animateRotate : true
      animateScale : false
    return

  return matchAnalysisChart

MatchAnalysisChart.$inject = [
]

app.factory('MatchAnalysisChart', MatchAnalysisChart)