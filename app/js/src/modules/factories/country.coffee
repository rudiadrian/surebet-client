Country = ($resource, APP_URLS) ->
  return $resource APP_URLS.api + '/countries/:id', id: '@id', {
    leagues:
      method: 'GET'
      url: APP_URLS.api + '/countries/:id/leagues_seasons/'
      params: {id: '@id'}
      isArray: true
  }

Country.$inject = [
  '$resource'
  'APP_URLS'
]

app.factory('Country', Country)