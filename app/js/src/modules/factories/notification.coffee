Notification = ($resource, APP_URLS) ->
  return $resource APP_URLS.api + '/notifications/:id', id: '@id', {
    read:
      method: 'GET'
      url: APP_URLS.api + '/notifications/read'
      isArray: true
    unread:
      method: 'GET'
      url: APP_URLS.api + '/notifications/unread'
      isArray: true
    watch_notifications:
      method: 'GET'
      url: APP_URLS.api + '/notifications/watch_notifications/'
      isArray: true
      ignoreLoadingBar: true
    mark_all_as_read:
      method: 'GET'
      url: APP_URLS.api + '/notifications/mark_all_as_read'
      isArray: false
    mark_all_as_unread:
      method: 'GET'
      url: APP_URLS.api + '/notifications/mark_all_as_unread'
      isArray: false
    mark_as_read:
      method: 'GET'
      url: APP_URLS.api + '/notifications/:id/mark_as_read'
      isArray: false
      params:
        id: '@id'
    mark_as_unread:
      method: 'GET'
      url: APP_URLS.api + '/notifications/:id/mark_as_unread'
      isArray: false
      params:
        id: '@id'
  }

Notification.$inject = [
  '$resource'
  'APP_URLS'
]

app.factory('Notification', Notification)