MatchAnalysis = ($resource, APP_URLS) ->
  return $resource APP_URLS.api + '/match_analyses/:id', id: '@id', {
    query:
      method: 'GET'
      isArray: false

    highest_outcomes:
      method: 'GET'
      url: APP_URLS.api + '/match_analyses/highest_outcomes/'
      isArray: true
  }

MatchAnalysis.$inject = [
  '$resource'
  'APP_URLS'
]

app.factory('MatchAnalysis', MatchAnalysis)