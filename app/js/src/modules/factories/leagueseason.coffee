LeagueSeason = ($resource, APP_URLS) ->
  return $resource APP_URLS.api + '/leagueseasons/:id?league__id=:leagueID&season__id=:seasonID', {
    id: '@id'
    leagueID: '@leagueID'
    seasonID: '@seasonID'
  }, {
    standings:
      method: 'GET'
      url: APP_URLS.api + '/leagueseasons/:id/standings'
      params:
        id: '@id'
      isArray: true
  }

LeagueSeason.$inject = [
  '$resource'
  'APP_URLS'
]

app.factory('LeagueSeason', LeagueSeason)