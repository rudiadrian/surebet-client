metisMenu = ->
  link = (scope, element, attrs) ->
    $(element).metisMenu()

  directive =
    link: link
    restrict: 'EA'

  return directive

app.directive('metisMenu', metisMenu)