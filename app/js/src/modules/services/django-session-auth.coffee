djangoSessionAuth = (
  $q
  $http
  $rootScope
  $cookies
  APP_ERRORS
  APP_URLS
  ) ->
  authService =
    API_URL: APP_URLS.api + '/auth'

    request: (url, method, data, params) ->
      deferred = $q.defer()

      $http({
        url: @API_URL + url
        withCredentials: true
        headers: {'X-CSRFToken': $cookies['csrftoken']},
        method: method || "GET"
        params: params || {}
        data: data || {}
      })
      .success angular.bind @, (data, status, headers, config) ->
        deferred.resolve(data, status)
        return
      .error angular.bind @, (data, status, headers, config) ->
        deferred.reject(data, status, headers, config)
        return

      return deferred.promise

    login: (username, password) ->
      that = @
      credentials =
        'username': username
        'password': password

      return that.request('/login/', 'POST', credentials)
        .then (userData) ->
          $rootScope.$broadcast 'djangoSessionAuth.loggedIn', userData
          return

    logout: ->
      that = @
      return that.request('/logout/', 'POST')
        .then ->
          $rootScope.$broadcast "djangoSessionAuth.loggedOut"
          return

    updateProfile: (userData) ->
      that = @
      return that.request('/user/', 'PATCH', userData)

    register: (userData) ->
      that = @
      return that.request('/register/', 'POST', userData)

    getAuthenticationStatusPromise: ->
      that = @
      deferred = $q.defer()

      that.request('/user/', 'GET')
      .then (userData) ->
        deferred.resolve()
        $rootScope.loggedUser = userData
        return
      , ->
        deferred.reject(APP_ERRORS.not_logged_in)
        $rootScope.loggedUser = null
        return

      return deferred.promise

  return authService

djangoSessionAuth.$inject = [
  '$q'
  '$http'
  '$rootScope'
  '$cookies'
  'APP_ERRORS'
  'APP_URLS'
]

app.service('djangoSessionAuth', djangoSessionAuth)