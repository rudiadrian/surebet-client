APP_ERRORS =
  not_logged_in: 'User is not logged in.'

APP_URLS =
  api: 'http://localhost:8000/api'

app
  .constant('APP_ERRORS', APP_ERRORS)
  .constant('APP_URLS', APP_URLS)