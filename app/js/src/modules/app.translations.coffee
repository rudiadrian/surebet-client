translations = ($translateProvider) ->

  $translateProvider.useStaticFilesLoader({
    prefix: 'static/i18n/',
    suffix: '.json'
  })

  $translateProvider.preferredLanguage 'pl'

translations.$inject = [
  '$translateProvider'
]

app.config(translations)