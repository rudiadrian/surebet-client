app = angular.module 'surebet', [
  'ui.router'
  'angular-loading-bar'
  'ngCookies'
  'ngAnimate'
  'toaster'
  'pascalprecht.translate'
  'ngResource'
  'ngTable'
  'ui.bootstrap'
  'tc.chartjs'
  'angular.filter'
  'emguo.poller'
]